﻿using UnityEngine;
using UnityEditor;

public class CreateAssetBundles : MonoBehaviour
{
	[MenuItem("Assets/Build AssetBundles")]
	static void BuildAssetBundles() {
		BuildPipeline.BuildAssetBundles("Assets/AssetBundles", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
	}
}
