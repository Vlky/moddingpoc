﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		var rigidbody = transform.GetComponent<Rigidbody>();
        rigidbody.AddTorque(new Vector3(0, 0, 100), ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
