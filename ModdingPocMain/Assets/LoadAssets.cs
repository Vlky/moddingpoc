﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text;
using UnityEngine;

public class ShipModel
{
    public string id;
    public string name;
    public GameObject prefab;
    public Texture2D interior;
    
}
public class LoadAssets : MonoBehaviour
{
	public string assetBundleName;
    public Dictionary<string, GameObject> shipPrefabs;
    public Dictionary<string, Texture2D> shipInteriorTemplates;
    public bool loading;

    private Dictionary<string, ShipModel> shipModels;

    private Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(256, 256, TextureFormat.ARGB32, false, false);
            tex.filterMode = FilterMode.Point;
            tex.wrapMode = TextureWrapMode.Clamp;
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }
    private XmlDocument ReadXML(string filePath)
    {
        var text = ReadTextFile(filePath);
        var xml = new XmlDocument();
        xml.LoadXml(text);
        return xml;
    }
    private string ReadTextFile(string filePath)
    {
        StreamReader input = new StreamReader(filePath);

        var builder = new StringBuilder();

        while (!input.EndOfStream)
        {
            builder.AppendLine(input.ReadLine());
        }

        input.Close();

        return builder.ToString();
    }

    private void ParseShips(XmlDocument xml)
    {
        var groupNode = xml.ChildNodes[0];

        var count = groupNode.ChildNodes.Count;

        for (var i = 0; i < count; i++)
        {
            var node = groupNode.ChildNodes[i];
            
            var model = new ShipModel()
            {
                id = node.Name,
                name = node.Attributes["inGameName"].Value,
                interior = shipInteriorTemplates[node.Attributes["interiorFileName"].Value],
                prefab = shipPrefabs[node.Attributes["prefabFileName"].Value]
            };

            shipModels.Add(model.id, model);
        }
    }
    private void LoadShipInteriorTemplates()
    {
        shipInteriorTemplates = new Dictionary<string, Texture2D>();
        var directoryInfo = new DirectoryInfo(Application.streamingAssetsPath);
        var directories = directoryInfo.GetDirectories();
        foreach (var directory in directories)
        {
            var path = Path.Combine(directory.FullName, "ShipInteriorTemplates");
            if (!Directory.Exists(path))
            {
                continue;
            }
            var shipDirectoryInfo = new DirectoryInfo(path);
            FileInfo[] files = shipDirectoryInfo.GetFiles("*.png");
            foreach (var png in files)
            {
                Debug.Log(png.FullName);
                var name = Path.GetFileNameWithoutExtension(png.Name);
                shipInteriorTemplates.Add(name, LoadPNG(png.FullName));
                Debug.Log("added " + name);
            }
        }
    }
    private void LoadShipModels()
    {
        shipModels = new Dictionary<string, ShipModel>();
        var directoryInfo = new DirectoryInfo(Application.streamingAssetsPath);
        var directories = directoryInfo.GetDirectories();
        foreach (var directory in directories)
        {
            var path = Path.Combine(directory.FullName, "ShipDescriptions");
            if (!Directory.Exists(path))
            {
                continue;
            }
            var shipDirectoryInfo = new DirectoryInfo(path);
            FileInfo[] files = shipDirectoryInfo.GetFiles("*.xml");
            foreach (var xml in files)
            {
                Debug.Log(xml.FullName);
                ParseShips(ReadXML(xml.FullName));
            }
        }
    }

    IEnumerator Start()
    {
        yield return new WaitForSeconds(1);
        loading = true;

        LoadShipInteriorTemplates();
        yield return LoadShipPrefabs();
        LoadShipModels();
        
        loading = false;
        var vector = transform.position;
        var rotation = transform.rotation;

        foreach (var model in shipModels.Values)
        {
            Instantiate(model.prefab, vector, rotation);
            vector = vector + new Vector3(1,0,0);
        }
    }

    IEnumerator LoadShipPrefabs()
    {
        shipPrefabs = new Dictionary<string, GameObject>();
        var directoryInfo = new DirectoryInfo(Application.streamingAssetsPath);
        var directories = directoryInfo.GetDirectories();
        foreach (var directory in directories)
        {
            var directoryPath = Path.Combine(directory.FullName, "ShipPrefabs");
            if (!Directory.Exists(directoryPath))
            {
                continue;
            }
            var prefabDirectoryInfo = new DirectoryInfo(directoryPath);
            FileInfo[] files = prefabDirectoryInfo.GetFiles("*.");
            foreach (var file in files)
            {
                var prefabPath = Path.Combine(directoryPath, file.Name);
                Debug.Log(prefabPath);
                var bundleLoadRequest = AssetBundle.LoadFromFileAsync(prefabPath);
                yield return bundleLoadRequest;

                var bundle = bundleLoadRequest.assetBundle;

                if (bundle == null)
                {
                    Debug.Log(prefabPath + " is not an assetbundle!");
                    yield break;
                }

                var prefabs = bundle.LoadAllAssets<GameObject>();
                yield return prefabs;

                foreach (var go in prefabs)
                {
                    shipPrefabs.Add(go.name, go);
                    Debug.Log("added " + go.name);
                }

                bundle.Unload(false);
            }
        }

        yield return null;
    }
}
